import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import UserTests from '@/components/tests/theTest'
import UserLogin from '@/components/auth/theLogin'
import TestDetail from '@/components/tests/theTestDetail'
import UserCreate from '@/components/auth/theRegistration'
import Tasks from '@/components/tasks/theTasks'
import TaskCreate from '@/components/tasks/theTaskCreate'
import TestCreate from '@/components/tests/theTestCreate'


Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/tests/',
      name: 'UserTests',
      component: UserTests
    },
    {
      path: '/tests/:ID',
      name: 'test-detail',
      component: TestDetail
    },
    {
      path: '/login/',
      name: 'LoginUser',
      component: UserLogin
    },
    {
      path: '/registration/',
      name: 'registration-user',
      component: UserCreate
    },
    {
      path: '/tasks/',
      name: 'tasks-list',
      component: Tasks
    },
    {
      path: '/tasks/create',
      name: 'tasks-create',
      component: TaskCreate
    },
    {
      path: "/create/tests/",
      name: 'test-create',
      component: TestCreate
    }
  ]
})
